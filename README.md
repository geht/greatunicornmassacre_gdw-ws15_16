GameDevWeek
===========

https://fsi.hochschule-trier.de/dev/null/GameDevWeek  
https://fsi.hochschule-trier.de/dev/null/Great_Unicorn_Massacre  
https://github.com/GameDevWeek/GDW-2015-WS  

Download:  
https://cloud.fsi.hochschule-trier.de/index.php/s/52fI9nu3ntfcm37  
https://bitbucket.org/geht/greatunicornmassacre_gdw-ws15_16/downloads/  

some important links:  
https://fsi.hochschule-trier.de/dev/null/GameDevWeek/Programmieren  
https://fsi.hochschule-trier.de/dev/null/GameDevWeek/Eclipse  
https://fsi.hochschule-trier.de/dev/null/GameDevWeek/Maven  
https://github.com/GameDevWeek/CodeBase/wiki/Used-technologies  